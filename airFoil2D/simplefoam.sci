
function simpleFoamF(varargin)
	[lhs,rhs] = argn(0)
	if rhs >= 1 then
		error('wrong number of inputs')
	end
	if rhs == 0 then
		dir = unix_g('pwd');
	else
		dir = string(varargin(1))
	end
	chdir(dir)
	setenv('LD_LIBRARY_PATH','/home/aashay/gsoc/OpenFOAM-toolbox/etc')
	setenv('WM_PROJECT_DIR',dir)
	unix_w('/home/aashay/gsoc/OpenFOAM-toolbox/simpleFoam-static')
endfunction